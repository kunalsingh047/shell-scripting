#!/bin/bash
#set -xe
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
STATUS=`sudo systemctl status lighttpd|awk 'NR==3{ print $2}'`
if [ $STATUS == active ]
then
	echo -e "====================================================="
	echo -e "====${YELLOW}httpd${NC} service status is ${GREEN}${STATUS}${NC}==="
        echo -e "====================================================="
else
	echo -e "====================================================="
        echo -e "====${YELLOW}httpd${NC} service status is ${RED}${STATUS}${NC}====="
        echo -e "====================================================="
fi
if [ $STATUS == active ]
then
	echo -e "Do you want to stop/restart ${YELLOW}httpd${NC} service"
while :
do 
	read OPTION
	case $OPTION in
		stop)
			echo -e "stopping ${YELLOW}httpd${NC} service"
			echo -e "===================================="
			sudo systemctl stop lighttpd
			echo -e "${YELLOW}httpd${NC} service has been stopped"
			exit 0
			;;
		restart)
			echo -e "restarting ${YELLOW}httpd${NC} service"
			echo -e "======================================"
			sudo systemctl restart lighttpd
			echo -e "restarted ${YELLOW}httpd${NC} service"
			exit 0
			;;
		*)
			echo -e "Please provide the correct option."
			exit 1
			;;
	esac
done
else 
	echo -e "Do you want to start/nothing ${YELLOW}httpd${NC} service"
while :
do 
	read OPTION
	case $OPTION in
		start)
			echo -e "starting ${YELLOW}httpd${NC} service"
			echo -e "===================================="
			sudo systemctl start lighttpd
			sleep 5
			echo -e "${YELLOW}httpd${NC} service has been started"
			echo -e "============================================"
			exit 0
			;;
		nothing)
			echo -e "leaving ${YELLOW}httpd${NC} service in inactive state"
			exit 0
			;;
		*)
			echo -e "Please provide correct option"
			exit 1
			;;

	esac
done
fi




