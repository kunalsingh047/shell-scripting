#!/bin/bash
select opt in status start stop restart Quit
do
	case $opt in

		status)
			STATUS=`sudo systemctl status lighttpd|awk 'NR==3{ print $2 }'`
			echo -e "httpd status is $STATUS"
			;;

		start)
			echo -e "Starting httpd service...."
			echo -e "=========================="
			sudo systemctl start lighttpd
			echo -e "httpd service has been started."
			echo -e "==============================="
			;;
		stop)
			echo -e "Stoping httpd service...."
                        echo -e "=========================="
                        sudo systemctl stop lighttpd
                        echo -e "httpd service has been stoped."
                        echo -e "==============================="
                        ;;
		restart)
			echo -e "restarting httpd service...."
                        echo -e "=========================="
                        sudo systemctl start lighttpd
                        echo -e "httpd service has been restarted."
                        echo -e "==============================="
                        ;;
		Quit)
			exit 0
			;;
		*)
			echo -e "Please enter correct option"
			;;


	esac
done


