#!/bin/bash 
set -xe
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'
STATUS=`systemctl status lighttpd |awk 'NR==3{ print $2 }'`
echo -e "Current status of httpd service is $STATUS"
echo -e "============================================"
echo -e "============================================"
if [ $STATUS == active ]
then
	echo -e "Do you want to stop/restart httpd service"
	echo -e "============================================"
	read $OPTION
	if [ $OPTION=stop ]
	then
		echo -e "stoping httpd service"
		echo -e "============================================"
		sudo systemctl stop lighttpd
		echo -e "============================================"
		NEWSTATUS=`systemctl status lighttpd |awk 'NR==3{ print $2 }'`
	echo -e "======================================="
	echo -e  "HTTP STATUS IS ${RED}${NEWSTATUS}${NC}"
	echo -e "======================================"
	else
		echo -e "restarting httpd service"
		echo -e "============================================"
		sudo systemctl restart lighttpd
		NEWSTATUS=`systemctl status lighttpd |awk 'NR==3{ print $2 }'`
		echo -e "=================================================================="
		echo -e "restarted httpd service, current status ${GREEN}${NEWSTATUS}${NC} "
	fi

else
	echo -e  "==================================="
	echo -e  "HTTP STATUS IS ${RED}${STATUS}${NC}\n Do you want to start the httpd service ."
	echo -e "===================================="
	read $OPTION
	if [ $OPTION=start ]
	then
		echo -e "Starting httpd service"
		echo -e "========================="
		sudo systemctl start lighttpd
		sleep 5
		NEWSTATUS=`systemctl status lighttpd |awk 'NR==3{ print $2 }'`
		echo -e "Httpd service status is ${GREEN}${NEWSTATUS}${NC}"
	else 
		echo -e "Leaving httpd service stop"
	fi
fi
